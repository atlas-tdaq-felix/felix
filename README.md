FELIX
-----

The is the source code for the [FELIX project](https://atlas-project-felix.web.cern.ch/atlas-project-felix/). It is divided into 4 directories:

1. Documents: Documentation in general
2. Hardware: custom developed hardware
3. firmware: Everything related to the definition and build of the firmware, including the register map to address the firmware. 
4. software: Everything related to the readout system, including tools for various software. 

NOTE: The GIT repository is divided in different subrepositories for software, firmware, hardware and documents.
The subrepositories can be cloned with the following command:

./clone_all.sh ssh

The software repository itself is also an empty placeholder with scripts to clone each of the software components.
it contains a README with further instructions on how to proceed.


Register Map
------------
The register map is defined under firmware/sources/templates/registers-x.y.yaml, and generated documentation
under Documents/regmap. To access the registers one uses the regmap library generated under software/regmap,
using the WupperCodeGen defined under software/wuppercodegen.


## Links to subrepositories

| Repository          | Link                                                                            |
|:--------------------|:--------------------------------------------------------------------------------|
| [software]          | https://gitlab.cern.ch/atlas-tdaq-felix/software                                |
| [firmware]          | https://gitlab.cern.ch/atlas-tdaq-felix/firmware                                |
| [documents]         | https://gitlab.cern.ch/atlas-tdaq-felix/documents                               |
| [hardware]          | https://gitlab.cern.ch/atlas-tdaq-felix/hardware                                |
