#!/bin/bash

usage()
{
  echo "usage: $(basename $0)  ssh|krb5|https"
  exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

. ./clone_software.sh $1
. ./clone_firmware.sh $1
. ./clone_documents.sh $1
. ./clone_hardware.sh $1
