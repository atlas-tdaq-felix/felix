#!/bin/bash

usage()
{
  echo "usage: $(basename $0)  ssh|krb5|https"
  exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

if [ $1 = "krb5" ]; then
  PREFIX="https://:@gitlab.cern.ch:8443"
  POSTFIX=""
elif [[ $1 = "https" ]]; then
  PREFIX="https://gitlab.cern.ch"
  POSTFIX=".git"
else
  PREFIX="ssh://git@gitlab.cern.ch:7999"
  POSTFIX=""
fi

git clone ${PREFIX}/atlas-tdaq-felix/documents${POSTFIX}

